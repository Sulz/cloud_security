import sys

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"

    key1 = raw_input("Enter the first of two keys [k1 = '5'if blank]:\n")
    key2 = raw_input("Enter the second of two keys [k2 = '3' if blank]:\n")

    accepted_keys = [1, 3, 5, 7, 9, 11, 17, 21, 25]

    if not key1 and not key2:
        key1 = 5
        key2 = 3
    elif key1 and not key2:
        key2 = 3
    elif key2 and not key1:
        key1 = 5

    else:
        key1 = int(key1)
        key2 = int(key2)

    # if key1 or key2 not in accepted_keys:
    #     print "The keys must be of the following:", accepted_keys
    #     return 0

    # assert key1 * key2 == 1, "The two keys must be the mod26 inverse of each other."
        # if key1 * key2 != 1:


    if str(sys.argv[1]) == '-enc':
        _encryption(key1, key2)

    elif str(sys.argv[1]) == '-dec':
        _decryption(key1, key2)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a string for encryption:\n")
        cipher = _encryption(key1, key2, src)
        _decryption(key1, key2, cipher)


def _encryption(key1, key2, src = 0):
    """File I/O"""
    source_plaintext = open('source_plaintext.txt', 'r')
    plaintext = source_plaintext.read()
    source_plaintext.close()
    plaintext = plaintext.rstrip().lower()

    if src != 0:
        plaintext = src.lower()

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    ciphertext = []
    cipherdex = []
    for plain_letter in plaintext:
        if plain_letter == " ":
            ciphertext.append(" ")
        else:
            for index in range(len(alphabet)):
                if alphabet[index] == plain_letter:
                    break

            cipher_index = ((key1 * index) + key2) % 26
            cipherdex.append(cipher_index)
            ciphertext.append(alphabet[cipher_index])

    ciphertext = ''.join(ciphertext)
    cipher_file = open('cipher_text.txt', 'w')
    cipher_file.write(ciphertext)
    cipher_file.close()

    print "\nEncryption Complete"
    print "Source plaintext:\n ", plaintext
    print '\nCipher text:\n ', ciphertext

def _decryption(key1, key2, src = 0):
    source_ciphertext = open('cipher_text.txt', 'r')
    ciphertext = source_ciphertext.read()
    source_ciphertext.close()

    if src != 0:
        cipher_text = src

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    plaintext = []
    plaindex = []

    for cipher_letter in ciphertext:
        if cipher_letter == " ":
            plaintext.append(" ")
        else:
            for index in range(len(alphabet)):
                if alphabet[index] == cipher_letter:
                    break

            k1_inv = inverseMod(key1, 26)
            plain_index = ((k1_inv)*(index - key2)) % 26
            plaindex.append(plain_index)
            plaintext.append(alphabet[plain_index])


    plaintext = ''.join(plaintext)

    print "\nDecryption Complete"
    print 'Cipher text:\n ', ciphertext
    print "\nPlaintext:\n ", plaintext

def inverseMod(a, m = 26):
    for i in range(1,m):
        if ( m*i + 1) % a == 0:
            return ( m*i + 1) // a
    return None

if __name__ == "__main__":
    main()
