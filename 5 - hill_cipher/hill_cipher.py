import sys
import numpy as np
from fractions import gcd

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"

    key1 = []
    print "Please enter the key ([11, 8, 3, 7] if blank)"
    for i in range(4):
        key1.append(raw_input("(" + str(i + 1) + ") of 4:\n"))

    if key1[0] == '':
        key1 = [[11, 8], [3, 7]]
        key1 = np.matrix(key1)

    else:
        key1 = [[int(key1[0]), int(key1[1])], [int(key1[2]), int(key1[3])]]
        key1 = np.matrix(key1)

    if str(sys.argv[1]) == '-enc':
        check = _encryption(key1)
        if check == 1:
            print "The key you entered is not mod26 invertible"


    elif str(sys.argv[1]) == '-dec':
        _decryption(key1)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a string for encryption:\n")
        cipher = _encryption(key1, src)
        _decryption(key1, cipher)


def _encryption(key1, src = 0):

    """Ensure key is valid"""
    key1_det = np.linalg.det(key1) % 26
    key1_det = int(str(key1_det)[0])
    # print 'key1_det', key1_det
    if (gcd(key1_det, 26) != 1 and key1_det != 1) or inverseMod(key1_det) == None:
        return 1

    # assert gcd(key1_det, 26) == 1, \
        # "The key you have entered is not mod26 invertible."

    """File I/O"""
    source_plaintext = open('source_plaintext.txt', 'r')
    plaintext = source_plaintext.read()
    source_plaintext.close()
    plaintext = plaintext.rstrip().lower()

    if src != 0:
        plaintext = src.lower()

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    cipher_index = []
    ciphertext = []

    i = 0
    while i < len(plaintext):
        char1 = plaintext[i]

        if i + 1 < len(plaintext):
            char2 = plaintext[i + 1]

        i += 2

        for index in range(0, len(alphabet)):
            if alphabet[index] == char1:
                char1_val = index
                break

        for index in range(0, len(alphabet)):
            if alphabet[index] == char2:
                char2_val = index
                break

        vectors = [char1_val, char2_val]
        vectors = np.array(vectors)

        result = vectors * key1

        result = result.tolist()

        for each in result:
            for _ in each:
                cipher_index.append(_ % 26)
                ciphertext.append(alphabet[_%26])

    ciphertext = ''.join(ciphertext)
    cipher_file = open('cipher_text.txt', 'w')
    cipher_file.write(ciphertext)
    cipher_file.close()

    print "\nEncryption Complete"
    print "Source plaintext:\n ", plaintext
    print '\nCipher text:\n ', ciphertext

def _decryption(key1, src = 0):
    source_ciphertext = open('cipher_text.txt', 'r')
    ciphertext = source_ciphertext.read()
    source_ciphertext.close()

    if src != 0:
        cipher_text = src

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    plaintext = []
    plaindex = []

    key_vals = []
    for each in key1.tolist():
        for keyval in each:
            key_vals.append(keyval)


    inverse_key = [[key_vals[3], -key_vals[1] % 26], [-key_vals[2] % 26, key_vals[0]]]

    inverse_key = np.array(inverse_key)

    key_det = np.linalg.det(key1) % 26

    key_det = int(str(key_det)[0])

    inverse_det = inverseMod(key_det, 26)

    inverse_key = inverse_det * inverse_key
    inverse_key = inverse_key.tolist()

    dec_key = []
    for each in inverse_key:
        for _ in each:
            dec_key.append(_ % 26)

    dec_key = [[dec_key[0], dec_key[1]], [dec_key[2], dec_key[3]]]
    dec_key = np.array(dec_key)

    i = 0
    while i < len(ciphertext):
        char1 = ciphertext[i]
        if i + 1 < len(ciphertext):
            char2 = ciphertext[i + 1]

        i += 2

        for index in range(0, len(alphabet)):
            if alphabet[index] == char1:
                char1_val = index
                break

        for index in range(0, len(alphabet)):
            if alphabet[index] == char2:
                char2_val = index
                break

        vectors = [char1_val, char2_val]
        vectors = np.array(vectors)

        result = np.dot(vectors, dec_key)
        result = result.tolist()

        for each in result:
            plaindex.append(each % 26)
            plaintext.append(alphabet[each % 26])

    plaintext = ''.join(plaintext)

    # print 'len(ciphertext)', len(ciphertext)
    # if len(ciphertext) % 2 != 0:
        # print "here"
        # plaintext = plaintext[:-1]

    print "\nDecryption Complete"
    print 'Cipher text:\n ', ciphertext
    print "\nPlaintext:\n ", plaintext

def inverseMod(num, mod = 26):
    for i in range(1, mod):
        if not (i*mod + 1) % num:
            return (i * mod + 1) // num
    return None

if __name__ == "__main__":
    main()
