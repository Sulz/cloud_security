# Cloud Security
This repo contains a set of code developed for the Cloud Security course at RMIT.

It contains code for the following ciphers / encryption schemes:

 - Spartan Scytale 

 - Transposition ciphers

 - Shift ciphers

 - Affine cipher

 - Hill cipher

 - Vigenere cipher

 - German ADFGVX cipher

 - The SDES Substitution Box

 - RSA key generation using a fast algorithm for calculating exponents.

The assignment contains code for full SDES and RSA.


## To run
Each option should have one .py file, simply run it using python and follow any prompts.
