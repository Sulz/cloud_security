import sys
from random import randint
from fractions import gcd
from decimal import Decimal

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify keygen [-kg] or fast-exp [-fe]\n"

    if str(sys.argv[1]) == '-kg':
        _determine_d()

    elif str(sys.argv[1]) == '-fe':
        _get_fast_exp()

def _get_fast_exp():
    base = raw_input("Enter a base number [214]:\n")
    power = raw_input("Enter the exponent [17321]:\n")

    if not base and not power:
        base = 214
        power = 17321

    elif not base:
        base = 214
        power = int(power)

    elif not power:
        power = 17321
        base = int(base)

    else:
        base = int(base)
        power = int(power)

    fast_exp(base, power)

def bits_of(m):
    """
    Binary digits of n, from the least significant bit.

    >>> list(bits_of(11))
    [1, 1, 0, 1]
    """
    n=int(m)
    while n:
        yield n & 1
        n >>= 1

def fast_exp(x, n):
    """
    Compute x^n, using the binary expansion of n.
    """

    print "\nStarting fast exponentiation"
    if len(str(x)) < 10:
        print "Solving:", str(x) + "^" +str(n)
    # else:
        # print "Solving a number", str(len(str(x))), "characters long, to the power of " + str(n)
    result = 1
    partial = x

    for bit in list(bits_of(n)):
        if bit:
            result *= partial

        partial = partial**2

    print "Finished fast exponentiation"
    if len(str(result)) > 10:
        print "{:.2E}".format(Decimal(result))

    else:
        print result

def _determine_d():
    e = raw_input("Enter an 'e' [101]:\n")
    phi_n = raw_input("Enter Phi_n [288]:\n")
    # q = raw_input(" q [719]:\n")


    if not phi_n and not e:
        e = 101
        phi_n = 288

    elif not e:
        e = 101
        phi_n = int(phi_n)

    elif not phi_n:
        phi_n = 288
        e = int(e)

    else:
        phi_n = int(phi_n)
        e = int(e)
        
    # for i in range(100, phi_n):
        # if gcd(i, phi_n) == 1:
            # break
    # print i

    while 1:
        d = randint(1, phi_n)
        # print "testing d =", d
        product = e * d

        if product % phi_n == 1:
            break

    print "Private key d is:", d

if __name__ == "__main__":
    main()
