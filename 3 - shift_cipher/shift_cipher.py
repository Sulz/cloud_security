import sys

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"


    key = raw_input("Enter the key ['5' if blank]:\n")
    if not key:
        key = 5
    else:
        key = int(key)
    if str(sys.argv[1]) == '-enc':
        _encryption(key)

    elif str(sys.argv[1]) == '-dec':
        _decryption(key)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a string for encryption:\n")
        cipher = _encryption(key, src)
        _decryption(key, cipher)


def _encryption(key, src = 0):
    """File I/O"""
    source_plaintext = open('source_plaintext.txt', 'r')
    plaintext = source_plaintext.read()
    source_plaintext.close()
    plaintext = plaintext.rstrip()

    if src != 0:
        plaintext = src

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    ciphertext = []
    cipherdex = []
    for plain_letter in plaintext:
        if plain_letter == " ":
            ciphertext.append(" ")
        else:
            for index in range(len(alphabet)):
                if alphabet[index] == plain_letter:
                    break

            cipher_index = (index + key) % 26
            cipherdex.append(cipher_index)
            ciphertext.append(alphabet[cipher_index])

    ciphertext = ''.join(ciphertext)
    cipher_file = open('cipher_text.txt', 'w')
    cipher_file.write(ciphertext)
    cipher_file.close()

    print "\nEncryption Complete"
    print "Source plaintext:\n ", plaintext
    print '\nCipher text:\n ', ciphertext



def _decryption(key, src = 0):
    source_ciphertext = open('cipher_text.txt', 'r')
    ciphertext = source_ciphertext.read()
    source_ciphertext.close()

    if src != 0:
        cipher_text = src

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    plaintext = []
    plaindex = []

    for cipher_letter in ciphertext:
        if cipher_letter == " ":
            plaintext.append(" ")
        else:
            for index in range(len(alphabet)):
                if alphabet[index] == cipher_letter:
                    break

            plain_index = (index - key) % 26
            plaindex.append(plain_index)

            plaintext.append(alphabet[plain_index])


    plaintext = ''.join(plaintext)

    print "\nDecryption Complete"
    print 'Cipher text:\n ', ciphertext
    print "\nPlaintext:\n ", plaintext


if __name__ == "__main__":
    main()
