import sys

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"

    key = raw_input("Enter the key ['venus' if blank]:\n")
    if not key:
        key = "venus"

    if str(sys.argv[1]) == '-enc':
        _encryption(key)

    elif str(sys.argv[1]) == '-dec':
        _decryption(key)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a string for encryption:\n")
        cipher = _encryption(key, src)
        _decryption(key, cipher)


def _encryption(key, src = 0):
    """File I/O"""
    source_plaintext = open('source_plaintext.txt', 'r')
    plaintext = source_plaintext.read()
    source_plaintext.close()
    plaintext = plaintext.rstrip().lower()

    if src != 0:
        plaintext = src.lower()

    ciphertext = []
    keystring = [[''] for i in range(len(plaintext))]

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    for i in range(len(plaintext)):
        if i < len(key):
            keystring[i] = key[i]
        else:
            for j in range(len(plaintext)):
                if j * len(key) > i:
                    break
            keystring[i] = key[i - (j * len(key))]


        char = plaintext[i]
        char_dex = get_character_index(char)
        key_dex = get_character_index(keystring[i])

        cipherdex = (char_dex + key_dex) % 26

        ciphertext.append(alphabet[cipherdex])


    ciphertext = ''.join(ciphertext)
    cipher_file = open('cipher_text.txt', 'w')
    cipher_file.write(ciphertext)
    cipher_file.close()

    print "\nEncryption Complete"
    print "Source plaintext:\n ", plaintext
    print '\nCipher text:\n ', ciphertext

def _decryption(key,  src = 0):
    source_ciphertext = open('cipher_text.txt', 'r')
    ciphertext = source_ciphertext.read()
    source_ciphertext.close()

    if src != 0:
        cipher_text = src

    plaintext = []
    plaindex = []

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    keystring = [[''] for i in range(len(ciphertext))]

    for i in range(len(ciphertext)):
        if i < len(key):
            keystring[i] = key[i]
        else:
            for j in range(len(ciphertext)):
                if j * len(key) > i:
                    break
            keystring[i] = key[i - (j * len(key))]

        cipher_char = ciphertext[i]
        key_char = keystring[i]

        cipherdex = get_character_index(cipher_char)
        key_dex = get_character_index(key_char)

        plaindex = (cipherdex - key_dex) % 26

        plaintext.append(alphabet[plaindex])

    plaintext = ''.join(plaintext)

    print "\nDecryption Complete"
    print 'Cipher text:\n ', ciphertext
    print "\nPlaintext:\n ", plaintext

def inverseMod(a, m = 26):
    for i in range(1,m):
        if ( m*i + 1) % a == 0:
            return ( m*i + 1) // a
    return None


def get_character_index(character):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    i = 0
    for each in alphabet:
        if each == character:
            return i
        else:
            i += 1

if __name__ == "__main__":
    main()
