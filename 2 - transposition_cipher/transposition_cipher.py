import sys

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"


    key = raw_input("Enter the key ['stock' if blank]:\n")
    if not key:
        key = 'stock'

    if str(sys.argv[1]) == '-enc':
        _encryption(key)

    elif str(sys.argv[1]) == '-dec':
        _decryption(key)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a string for encryption:\n")
        cipher = _encryption(key, src)
        _decryption(key, cipher)

def _encryption(key, src = 0):
    """File I/O"""
    source_plaintext = open('source_plaintext.txt', 'r')
    plaintext = source_plaintext.read()
    source_plaintext.close()
    plaintext = plaintext.rstrip()

    if src != 0:
        plaintext = src


    key = key.lower()
    num_cols = len(key)
    len_source = len(plaintext)

    key_list = []
    for i in range(num_cols):
        key_list.append(key[i])

    cipher_blocks = []
    for i in range(num_cols):
        cipher_blocks.append("")

    for i in range(num_cols):
        j = i
        while j < len(plaintext):
            cipher_blocks[i] += plaintext[j]
            j += num_cols

    # print 'cipher_blocks', cipher_blocks

    final_cipher = {}
    i = 0
    for each in key_list:
        final_cipher[each] = cipher_blocks[i]
        i += 1

    # print 'final_cipher', final_cipher

    cipher_text = []
    key_list.sort()

    """Builds the ciphertext by concatenating columns.
        Slides concatenates rows."""
    # OG:
    # for each in key_list:
    #     cipher_text.append(final_cipher[each])

    """Builds by row."""
    done = 0
    i = 0
    j = 0
    while not done:
        i = j
        j += 1
        for each in key_list:
            if i < len(final_cipher[each]):
                cipher_text.append(final_cipher[each][i])

        # print len(cipher_text)
        if len(cipher_text) == len_source:
            done = 1
            break

    cipher_text = ''.join(cipher_text)
    encrypted_file = open('cipher_text.txt', 'w')
    encrypted_file.write(cipher_text)
    encrypted_file.close()

    print "\nEncryption complete."
    print "source text:\n", plaintext
    print "\ncipher text:\n", cipher_text, '\n\n'
    if src != 0:
        return cipher_text


def _decryption(key, src = 0):
    encrypted_file = open('cipher_text.txt', 'r')
    cipher_text = encrypted_file.read()
    encrypted_file.close()

    if src != 0:
        cipher_text = src

    cipher_len = len(cipher_text)
    key = key.lower()
    num_cols = len(key)

    block_size = cipher_len/len(key)
    rem = cipher_len % len(key)

    fewer_chars = []
    for i in range(len(key)):
        if i >= rem:
            fewer_chars.append(key[i])

    key_list = []
    for i in range(num_cols):
        key_list.append(key[i])

    plaintext = []
    dec_dict = {}
    key_list.sort()
    # print 'key_list, sorted: ', key_list
    # print 'fewer_chars', fewer_chars

    count = 0
    for k in range(len(key_list)):
        dec_dict[key_list[k]] = ""
        lim = block_size + 1
        if key_list[k] in fewer_chars:
            lim = block_size

        i = 0
        while len(dec_dict[key_list[k]]) < lim:
            if not key_list[k] in fewer_chars and\
                len(dec_dict[key_list[k]]) + 1 == lim:

                # i = i - (len(fewer_chars))

                dec_dict[key_list[k]] += cipher_text[-(len(key_list) - len(fewer_chars)) + count]

                count += 1

            elif i + k < cipher_len:
                    dec_dict[key_list[k]] += cipher_text[i + k]

                    i += num_cols

    # print 'dec_dict\n', dec_dict

    nearly_plain = []
    for each in key:
        nearly_plain.append(dec_dict[each])
    # print 'nearly_plain', nearly_plain

    done = 0
    index = 0
    while not done:
        for i in range(len(nearly_plain)):
            if index < len(nearly_plain[i]):
                plaintext.append(nearly_plain[i][index])

        if len(plaintext) == len(cipher_text):
            done = 1
            break
        index += 1

    plaintext = ''.join(plaintext)

    decrypted_file = open("decrypted_cipher.txt", 'w')
    decrypted_file.write(plaintext)
    decrypted_file.close()

    original_plaintext = open("source_plaintext.txt", 'r')

    print "\nDecryption Complete.\n"
    if not src:
        print "original plaintext:\n",  original_plaintext.read()

    print "\nCipher text:\n", cipher_text
    print "\nDecrypted cipher text:\n", plaintext, '\n\n'

if __name__ == "__main__":
    main()
