import sys


"""Key needs to define rows, not columns!"""

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"

    key = raw_input("Enter the key [5 if blank]:\n")
    if not key:
        key = 5
    else:
        key = int(key)

    x = float(3)%float(26)
    # x = float(3)/float(26)

    # x = float(26)%float(3)

    print x
    # print 3/float(26)

    if str(sys.argv[1]) == '-enc':
        scytale_encryption(key)

    elif str(sys.argv[1]) == '-dec':
        scytale_decryption(key)

def scytale_encryption(key):
    source_plaintext = open('source_plaintext.txt', 'r')
    plaintext = source_plaintext.read()
    source_plaintext.close()

    plaintext = plaintext.rstrip()

    cipher_text = []
    indexed = []
    count = len(plaintext)
    val = count/key
    rem = count % key

    # rows = [[] for i in range(key)]
    # print rows
    # i = 0
    # while i < len(plaintext):
    #     j = 0
    #     rows[j].append(plaintext[i])
    #
    #     i += 1
    # print rows

    # OG:
    # print 'length of string:', count
    # print 'length/key', val
    # print 'rem', rem
    done = 0
    for i in range(count):
        j = i
        while j < count:
            if not j in indexed:
                cipher_text.append(plaintext[j])
                # indexed.append(j)
            if rem:
                j += val + 1
            else:
                j += val

        if len(cipher_text) == count:
            done = 1
            break

    cipher_text = ''.join(cipher_text)
    encrypted_file = open('cipher_text.txt', 'w')
    encrypted_file.write(cipher_text)
    encrypted_file.close()

    print "\nsource text:\n", plaintext
    print "\ncipher text:\n", cipher_text, '\n\n'

def scytale_decryption(key):
    encrypted_file = open('cipher_text.txt', 'r')
    cipher_text = encrypted_file.read()
    encrypted_file.close()

    indexed = []
    msg_len = len(cipher_text)

    plaintext = []
    for i in range(msg_len):
        plaintext.append("")

    val = msg_len/key
    rem = msg_len % key


    i, j, num_rounds = 0, 0, 0
    done = False
    while not done:
        while j < msg_len and i < msg_len:
            plaintext[j] = cipher_text[i]
            i += 1
            if rem:
                j += val + 1
            else:
                j += val

        if i == msg_len:
            done = True
            break

        num_rounds += 1
        j = num_rounds

    plaintext = ''.join(plaintext)

    decrypted_file = open("decrypted_cipher.txt", 'w')
    decrypted_file.write(plaintext)
    decrypted_file.close()

    print "\nCipher text:\n", cipher_text
    print "\nDecrypted cipher text:\n", plaintext, '\n\n'

if __name__ == "__main__":
    main()
