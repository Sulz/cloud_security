import sys

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"

    key = raw_input("Enter the 10-bit key [0010010111]:\n")
    if not key:
        key = '0010010111'
    else:
        key = str(key)
        assert len(key) == 10, \
            "\n\n The key must be exactly 10bits"

    if str(sys.argv[1]) == '-enc':
        _encryption(key)

    elif str(sys.argv[1]) == '-dec':
        _decryption(key)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a message to be encrypted:\n")
        _encryption(key, src)
        _decryption(key)


def _initial_permutation(src):

    b_map = [2, 6, 3, 1, 4, 8, 5, 7]

    permutated_src = [[0] for i in range(8)]
    i = 0
    for index in b_map:
        permutated_src[i] = src[index - 1]
        i += 1

    return permutated_src

def _generate_round_key(key):

    perm_key = _permutate_10(key)

    key_lhs = perm_key[:5]
    key_rhs = perm_key[5:]

    key_lhs, key_rhs = _lshift(key_lhs, key_rhs, 1)

    round_key_parts = key_lhs + key_rhs
    round_key1 = _permutate_8(round_key_parts)

    key_lhs, key_rhs = _lshift(key_lhs, key_rhs, 2)

    round_key_parts = key_lhs + key_rhs
    round_key2 = _permutate_8(round_key_parts)

    return round_key1, round_key2


def _permutate_10(key):
    b_map = [3, 5, 2, 7, 4, 10, 1, 9, 8, 6]
    permutated_key = [[0] for i in range(10)]
    i = 0
    for index in b_map:
        permutated_key[i] = key[index - 1]
        i += 1

    return ''.join(permutated_key)

def _lshift(key_lhs, key_rhs, shift):

    key_lhs = key_lhs[shift:] + key_lhs[:shift]
    key_rhs = key_rhs[shift:] + key_rhs[:shift]

    return key_lhs, key_rhs

def _permutate_8(key):
    b_map = [6, 3, 7, 4, 8, 5, 10, 9]

    permutated_key = [[0] for i in range(8)]
    i = 0
    for index in b_map:
        permutated_key[i] = key[index - 1]
        i += 1

    return ''.join(permutated_key)


def _key_function(key, src):
    src_lhs = src[:len(src)/2]
    src_rhs = src[(len(src)/2):]

    expanded_src = _expansion_permutation(src_rhs)

    xor_results = []
    for i in range(len(key)):
        xor_results.append(str(int(expanded_src[i]) ^ int(key[i])))

    xor_results = ''.join(xor_results)

    sub_lhs = xor_results[:(len(xor_results)/2)]
    lhs_bits = _sub_box(sub_lhs, 1)

    sub_rhs = xor_results[(len(xor_results)/2):]
    rhs_bits = _sub_box(sub_rhs, 2)

    p4_result = _permutate_4(lhs_bits + rhs_bits)

    xor_results = []
    for i in range(len(p4_result)):
        xor_results.append(str(int(src_lhs[i]) ^ int(p4_result[i])))

    xor_results = ''.join(xor_results)

    return xor_results + ''.join(src_rhs)

def _permutate_4(src):
    b_map = [2, 4, 3, 1]

    permutated_src = [[0] for i in range(4)]
    i = 0
    for index in b_map:
        permutated_src[i] = src[index - 1]
        i += 1

    return ''.join(permutated_src)

def _sub_box(src, sub_num):

    sub0 = [
        [1, 0, 3, 2],
        [3, 2, 1, 0],
        [0, 2, 1, 3],
        [3, 1, 3, 2]
    ]

    sub1 = [
        [0, 1, 2, 3],
        [2, 0, 1, 3],
        [3, 0, 1, 0],
        [2, 1, 0, 3]
    ]

    row = src[0] + src[3]
    col = src[1] + src[2]

    if sub_num == 1:
        sub_box = sub0
    elif sub_num == 2:
        sub_box = sub1

    return _int_to_bits(sub_box[int(row, 2)][int(col, 2)], 2)

def _int_to_bits(integer, num_bits):
    binary_int = bin(integer)
    binary_result = []
    found = 0
    for i in range(len(binary_int)):
        if found:
            binary_result.append(binary_int[i])

        if binary_int[i] == 'b':
            found = 1

    padding = []
    while len(padding) < num_bits - len(binary_result):
        padding.append('0')

    padding = padding + binary_result
    padding = ''.join(padding)

    return padding

def _expansion_permutation(src):
    b_map = [4, 1, 2, 3, 2, 3, 4, 1]

    expanded_src = [[0] for i in range(8)]
    i = 0
    for index in b_map:
        expanded_src[i] = src[index - 1]
        i += 1

    return ''.join(expanded_src)

def _sw_permutation(src):

    new_lhs = src[len(src)/2:]
    new_rhs = src[:len(src)/2]

    return new_lhs + new_rhs

def _inverse_IP(src):

    b_map = [4, 1, 3, 5, 7, 2, 8, 6]

    permutated_src = [[0] for i in range(8)]
    i = 0
    for index in b_map:
        permutated_src[i] = src[index - 1]
        i += 1

    return ''.join(permutated_src)

def _encryption(key, src = 0):
    """File I/O"""
    '''Loading from file'''
    if src == 0:
        source_plaintext = open('source_text.txt', 'r')
        plaintext = source_plaintext.read()
        source_plaintext.close()

    '''Manually specified string. Rewrite initial file.'''
    if src != 0:
        source_plaintext = open('source_text.txt', 'w')
        source_plaintext.write(src)
        plaintext = src

    '''Turn string into list of bytes'''
    src_binary = []
    for i in bytearray(plaintext, 'ascii'):
        src_binary.append(bin(i))

    '''Remove pythons formatting'''
    final_src_binary = []
    for each in src_binary:
        each = each[::-1]
        plain_binary = []
        found = 0
        for i in range(8):
            # print each[i]
            if i < len(each):
                if each[i] != 'b' and not found:
                    plain_binary.append(each[i])

                elif each[i] == 'b' or found:
                    found = 1
                    plain_binary.append('0')

            else:
                plain_binary.append('0')

        plain_binary = ''.join(plain_binary)
        plain_binary = plain_binary[::-1]
        final_src_binary.append(plain_binary)


    '''Write full source binary file.'''
    src_binary = open('source_binary.txt', 'w')
    src_binary.write(''.join(final_src_binary))
    src_binary.close()

    final_cipher = []
    final_cipher_bin = []

    '''Encrypt each of the binary blocks.'''
    for plain_binary in final_src_binary:
        cipher = _initial_permutation(plain_binary)

        key1, key2 = _generate_round_key(key)

        cipher = _key_function(key1, cipher)

        cipher = _sw_permutation(cipher)

        cipher = _key_function(key2, cipher)

        cipher = _inverse_IP(cipher)

        final_cipher_bin.append(cipher)
        final_cipher.append(chr(int(cipher, 2)))

    """Write the final cipher text to file"""
    cipher_file = open('cipher_text.txt', 'w')
    cipher_file.write(''.join(final_cipher))
    cipher_file.close()

    """Write the final cipher binary to file"""
    cipher_bin = open('cipher_binary.txt', 'w')
    cipher_bin.write(''.join(final_cipher_bin))
    cipher_bin.close()

    print "\n\nEncryption Complete\n"
    print "Source:"
    print " Plaintext:\n   ", plaintext
    print " Binary:\n", ''.join(final_src_binary)

    print "\nEncrypted:"
    print " Text:\n   ", ''.join(final_cipher)
    print ' Binary:\n', ''.join(final_cipher_bin)
    print "\n"

def _decryption(key):
    """Open cipher text (display purposes only)"""
    source_ciphertext = open('cipher_text.txt', 'r')
    ciphertext = source_ciphertext.read()
    source_ciphertext.close()

    """Open encypted message binary file."""
    src_cipher_bin_file = open('cipher_binary.txt', 'r')
    src_cipher_binary = src_cipher_bin_file.read()
    src_cipher_bin_file.close()

    """Ascertain number of 8-bit blocks in file"""
    num_blocks = len(src_cipher_binary)/8

    """Collect each byte."""
    src_bin_blocks = []
    for i in range(num_blocks):
        src_bin_blocks.append(src_cipher_binary[i*8:(i+1)*8])

    decrypted_binary = []
    decrypted_string = []

    """Decrypt each byte"""
    for binary in src_bin_blocks:
        # print 'Decryption\nbinary:', binary
        plaintext = _initial_permutation(binary)

        key1, key2 = _generate_round_key(key)

        plaintext = _key_function(key2, plaintext)

        plaintext = _sw_permutation(plaintext)

        plaintext = _key_function(key1, plaintext)

        plaintext = _inverse_IP(plaintext)

        decrypted_binary.append(plaintext)
        decrypted_string.append(chr(int(plaintext, 2)))

    print "\nDecryption Complete\n"
    print "Source cipher:"
    print " Text:\n   ", ciphertext
    print " Binary:\n", ''.join(src_bin_blocks)

    print "\nDecrypted:"
    print " Text:\n   ", ''.join(decrypted_string)
    print ' Binary:\n', ''.join(decrypted_binary)
    print "\n"


if __name__ == "__main__":
    main()
