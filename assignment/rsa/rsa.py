import sys
from fractions import gcd
from random import randint
from decimal import Decimal

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"

    print "Enter two distinct prime numbers as the keys"
    p = raw_input(" p [101]:\n")
    q = raw_input(" q [331]:\n")

    if not p and not q:
        p = 101
        q = 331

    elif not p:
        p = 101
        q = int(q)

    elif not q:
        q = 331
        p = int(p)

    else:
        p = int(p)
        q = int(q)

    if str(sys.argv[1]) == '-enc':
        _encryption(p, q)

    elif str(sys.argv[1]) == '-dec':
        _decryption(p, q)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a message to be encrypted:\n")
        _encryption(src)
        _decryption()

def _key_gen(p, q, enc = 0):
# print(randint(0, 9))
    print "\n____________________________________________"
    print "\n           Generating RSA Keys"
    print "____________________________________________"

    n = p*q
    print ' n', n

    phi_n = (p-1) * (q-1)
    print ' phi_n', phi_n

    """Replace with random int selection between 1-phi_n"""
    # for e in range(17, phi_n):
    while 1:
        e = randint(1, phi_n)
        if gcd(e, phi_n) == 1:
            break

    print ' e', e

    # for d in range(1, phi_n):
    while 1:
        d = randint(1, phi_n)
        product = e * d

        if product % phi_n == 1:
            break

    print ' d', d

    """File IO"""
    '''public key'''
    public_key = open('public_key.txt', 'w')
    public_key.write(str(n) + '\n')
    public_key.write(str(e) + '\n')
    public_key.write(str(d) + '\n')
    public_key.close()


    '''private key'''
    private_key = open('private_key.txt', 'w')
    private_key.write(str(p) + '\n')
    private_key.write(str(q) + '\n')
    private_key.write(str(d) + '\n')
    private_key.write(str(n) + '\n')
    private_key.write(str(e) + '\n')
    private_key.close()

    if enc:
        return n, e, d

    return d, n, e

def _encryption(p, q, src = 0):
    """File I/O"""
    '''Loading from file'''
    if src == 0:
        source_plaintext = open('source_plaintext.txt', 'r')
        plaintext = source_plaintext.read()
        source_plaintext.close()
        plaintext = plaintext.rstrip().lower()

    '''Manually specified string. Rewrite initial file.'''
    if src != 0:
        source_plaintext = open('source_plaintext.txt', 'w')
        source_plaintext.write(src)
        plaintext = src

    # print "Retrieving public key for encryption."
    n, e, d = _key_gen(p, q, enc=1)

    msg = []
    for i in range(len(plaintext)):
        msg.append(int(get_character_index(plaintext[i])))

    signed_plaintext = sign_message(msg, d)

    print "\n____________________________________________"
    print "\n           Encrypting"
    print "____________________________________________"

    i = 0
    cipher_text = []
    unsigned_cipher = []
    for each in signed_plaintext:
        if i % 2 == 0:
            cipher_text.append(long(fast_exp(long(each), e)))
            unsigned_cipher.append(cipher_text[-1])
        else:
            cipher_text.append(long(each))

        i += 1

    """Write the final cipher text to file"""
    cipher_file = open('cipher_text.txt', 'w')
    cipher_file.write(str(cipher_text))
    cipher_file.close()
    print "\n____________________________________________"
    print "\n           Encryption Complete"

    print "\nSource:"
    print " Plaintext:\n   ", plaintext
    print " Message:\n   ", msg

    legible_cipher = []
    for each in unsigned_cipher:
        legible_cipher.append("{:.2E}".format(Decimal(each)))

    print "\nEncrypted:"
    print " Unsigned Cipher:\n   ", legible_cipher
    print "\n____________________________________________"
    print "\n\n"

def _decryption(p, q):
    """Open cipher text (display purposes only)"""
    source_ciphertext = open('cipher_text.txt', 'r')
    ciphertext = source_ciphertext.read()
    source_ciphertext.close()

    # print "Retrieving private keys for decryption."


    verify_keys = open('private_key.txt', 'r')
    correct_p = int(verify_keys.readline())
    correct_q = int(verify_keys.readline())
    verify_keys.close()

    if correct_q != q or correct_p != p:
        # print "got p", p, "q, ", q
        # print "correct_p", correct_p, 'correct_q', correct_q
        # print "incorrect keys"
        d, n, e = _key_gen(p, q)

    else:
        # print "correct keys"
        private_keys = open('private_key.txt', 'r')
        p = private_keys.readline()
        q = private_keys.readline()
        d = private_keys.readline()
        n = private_keys.readline()
        e = private_keys.readline()
        private_keys.close()

    cipher = []

    i = 1
    while i < len(ciphertext) - 1:
        cipher_chars = []

        while ciphertext[i] != "," and i < len(ciphertext) - 1:
            cipher_chars.append(ciphertext[i])
            i += 1

        cipher.append("".join(cipher_chars))
        i += 1

    signed_message = []
    cipher_message = []
    message = []
    i = 0
    print "\n____________________________________________"
    print "\n               Decrypting"
    print "____________________________________________"
    for each in cipher:
        if i % 2 == 0:
            cipher_message.append(each)
            signed_message.append(fast_exp(long(each), int(d)) % int(n))
            message.append(int(signed_message[-1]))
        else:
            signed_message.append(long(each))

        i += 1

    print "\n____________________________________________"
    print "\n           Decryption Complete."
    print "____________________________________________"

    signed_plaintext = verify_signature(signed_message, e, n)

    plaintext = []
    corr = 1
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    # print 'message', message
    for each in message:
        # print 'each', each
        if each < len(alphabet):
            plaintext.append(alphabet[int(each)])
            corr = 0

    plaintext = "".join(plaintext)

    if not corr:
        print "\n__________________________________________________________"
        print "\n                   Results"
        print "Source cipher:"
        print " Text:\n   ", cipher_message


        print "\nDecrypted:"
        print " Text:\n   ", plaintext
        print ' Message:\n', message
        print "\n____________________________________________"
        print "\n\n"

    else:
        print "\n__________________________________________________________"
        print "\nDecrypted cipher could not be tranlated into legible text."
        print "\n__________________________________________________________"
        print "\n\n"

def sign_message(message, d):
    print "\n____________________________________________"
    print "\n          Signing Message"
    print "____________________________________________"

    signed_message = []
    for each in message:
        signed_message.append(long(each))
        # signed_message.append(long(long(each)**long(d)))
        signed_message.append(fast_exp(each, d))

    print "\n____________________________________________"
    print "\n   The plaintext message has been signed."
    print "____________________________________________"

    # print signed_message
    return signed_message


def verify_signature(signed_message, e, n):
    print "\n____________________________________________"
    print "\n          Verifying signature"
    print "____________________________________________"

    found = 0
    i = 0
    unsigned_msg = []
    for each in signed_message:
        i += 1
        if i % 2 != 0:
            unsigned_msg.append(each)
            msg = each

        else:
            # if (long(each)**int(e) % int(n)) != int(msg):
            if fast_exp(long(each), int(e)) % int(n) != long(msg):
                found = 1


    if found:
        print "\n____________________________________________"
        print "\n   Message tampered or incorrect key!"
        print "____________________________________________"
    else:
        print "\n____________________________________________"
        print "\n     Message has retained integrity."
        print "____________________________________________"
        print "\n"
    return signed_message

def get_character_index(character):
    # print 'character', character
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    i = 0
    for each in alphabet:
        if each == character:
            if len(str(i)) < 2:
                # print "returning:", str('0' + str(i))
                return str('0' + str(i))
            return str(i)
        else:
            i += 1


def inverseMod(num, mod = 26):
    for i in range(1, mod):
        if not (i*mod + 1) % num:
            return (i * mod + 1) // num
    return None

def bits_of(exp):
    """
        Present exponent as bits.
            >>> list(bits_of(11))
                [1, 1, 0, 1]
    """

    exp = long(exp)
    while exp:
        yield exp & 1
        exp >>= 1

def fast_exp(base, exp):
    """
    Computes x^n, using fast exponentiation algorithm.
    """

    print "\nStarting fast exponentiation"
    if len(str(base)) < 10:
        print "Solving:", str(base) + "^" +str(exp)
    else:
        print "Solving: ", "{:.2E}".format(Decimal(base)) + "^" + str(exp)
        print "The base is", str(len(str(base))), "characters long.."

    result = 1
    partial = base

    for bit in list(bits_of(exp)):
        if bit:
            result *= partial

        partial = partial**2

    print "Finished fast exponentiation"
    return result

if __name__ == "__main__":
    main()
