import sys

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify encryption [-enc] or decryption [-dec]\n"

    key = raw_input("Enter the key ['security' if blank]:\n")
    if not key:
        key = "security"

    if str(sys.argv[1]) == '-enc':
        _encryption(key)

    elif str(sys.argv[1]) == '-dec':
        _decryption(key)

    elif str(sys.argv[1] == '-man'):
        src = raw_input("Enter a string for encryption:\n")
        cipher = _encryption(key, src)
        _decryption(key, cipher)


def _encryption(key, src = 0):
    """File I/O"""
    source_plaintext = open('source_plaintext.txt', 'r')
    plaintext = source_plaintext.read()
    source_plaintext.close()
    plaintext = plaintext.rstrip().lower()

    sub_table = {
        "k":"AA",
        "9":"DA",
        "q":"FA",
        "e":"GA",
        "8":"VA",
        "u":"XA",
        "z":"AD",
        "b":"DD",
        "7":"FD",
        "v":"GD",
        "o":"VD",
        "4":"XD",
        "w":"AF",
        "6":"DF",
        "j":"FF",
        "y":"GF",
        "d":"VF",
        "1":"XF",
        "r":"AG",
        "c":"DG",
        "p":"FG",
        "3":"GG",
        "h":"VG",
        "s":"XG",
        "i":"AV",
        "l":"DV",
        "g":"FV",
        "a":"GV",
        "0":"VV",
        "t":"XV",
        "f":"AX",
        "5":"DX",
        "x":"FX",
        "n":"GX",
        "2":"VX",
        "m":"XX"
    }

    if src != 0:
        plaintext = src.lower()

    ciphertext = []
    keystring = [[''] for i in range(len(plaintext))]

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

    for i in range(len(plaintext)):
        ciphertext.append(sub_table[plaintext[i]])

    ciphertext = ''.join(ciphertext)
    # print 'ciphertext\n', ciphertext

    final_blocks = {}
    cipher_blocks = {}
    for i in range(len(key)):
        cipher_blocks[i] = ""
        final_blocks[key[i]] = ""

    # print 'final_blocks', final_blocks

    i = 0;
    while i < len(ciphertext):
        if i < len(cipher_blocks):
            cipher_blocks[i] += ciphertext[i]
        else:
            cipher_blocks[i % (len(key))] += ciphertext[i]
        i += 1

    i = 0
    for each in sorted(key):
        final_blocks[each] = cipher_blocks[i]
        i += 1
        # print 'each', each


    # print 'cipher_blocks\n', cipher_blocks
    # print 'final_blocks\n', final_blocks


    final_ciphertext = []
    i = 0
    j = 0

    for i in range(len(ciphertext)/len(key)):
        for each in key:
            final_ciphertext.append(final_blocks[each][i])

    ciphertext = final_ciphertext

    ciphertext = ''.join(ciphertext)
    cipher_file = open('cipher_text.txt', 'w')
    cipher_file.write(ciphertext)
    cipher_file.close()

    print "\nEncryption Complete"
    print "Source plaintext:\n ", plaintext
    print '\nCipher text:\n ', ciphertext

def _decryption(key,  src = 0):
    source_ciphertext = open('cipher_text.txt', 'r')
    ciphertext = source_ciphertext.read()
    source_ciphertext.close()

    if src != 0:
        cipher_text = src

    plaintext = []
    plaindex = []

    sub_table = {
        "k":"AA",
        "9":"DA",
        "q":"FA",
        "e":"GA",
        "8":"VA",
        "u":"XA",
        "z":"AD",
        "b":"DD",
        "7":"FD",
        "v":"GD",
        "o":"VD",
        "4":"XD",
        "w":"AF",
        "6":"DF",
        "j":"FF",
        "y":"GF",
        "d":"VF",
        "1":"XF",
        "r":"AG",
        "c":"DG",
        "p":"FG",
        "3":"GG",
        "h":"VG",
        "s":"XG",
        "i":"AV",
        "l":"DV",
        "g":"FV",
        "a":"GV",
        "0":"VV",
        "t":"XV",
        "f":"AX",
        "5":"DX",
        "x":"FX",
        "n":"GX",
        "2":"VX",
        "m":"XX"
    }

    dec_blocks = {}
    key_blocks = {}

    plaintext = []

    i = 0
    for each in sorted(key):
        dec_blocks[i] = ""
        key_blocks[each] = ""
        i += 1

    i = 0
    while i < len(ciphertext):
        dec_blocks[i % len(key)] += ciphertext[i]
        i += 1

    i = 0
    for each in key:
        key_blocks[each] = dec_blocks[i]
        i += 1

    nearly_plain = []

    for i in range(len(ciphertext)/len(key)):
        for each in sorted(key):
            nearly_plain.append(key_blocks[each][i])

    nearly_plain = "".join(nearly_plain)

    i = 0
    subs = []
    while i < len(nearly_plain):
        subs.append("".join(nearly_plain[i:i + 2]))
        i += 2

    plaintext = []
    for each in subs:
        for (key, value) in sub_table.iteritems():
            if each == value:
                plaintext.append(key)


    plaintext = ''.join(plaintext)

    print "\nDecryption Complete"
    print 'Cipher text:\n ', ciphertext
    print "\nPlaintext:\n ", plaintext

def inverseMod(a, m = 26):
    for i in range(1,m):
        if ( m*i + 1) % a == 0:
            return ( m*i + 1) // a
    return None


def get_character_index(character):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    i = 0
    for each in alphabet:
        if each == character:
            return i
        else:
            i += 1

if __name__ == "__main__":
    main()
