import sys

def main():
    assert len(sys.argv) == 2, \
        "\n\nBe sure to specify which S-Box! [-s0 || -s1]\n"

    # print "Enter the 4-bits to be substituted"
    bits = raw_input("Enter the 4 bits to be substituted:\n")
    # q = raw_input(" q [719]:\n")
    if not bits:
        bits = '1100'

    if str(sys.argv[1]) == '-s0':
        _sub_box(bits, 0)

    elif str(sys.argv[1]) == '-s1':
        _sub_box(bits, 1)

    print
def _sub_box(src, sub_num):

    sub0 = [
        [1, 0, 3, 2],
        [3, 2, 1, 0],
        [0, 2, 1, 3],
        [3, 1, 3, 2]
    ]

    sub1 = [
        [0, 1, 2, 3],
        [2, 0, 1, 3],
        [3, 0, 1, 0],
        [2, 1, 0, 3]
    ]

    row = src[0] + src[3]
    col = src[1] + src[2]

    if sub_num == 0:
        sub_box = sub0
        print "The result of S-Box 0 is:"
    elif sub_num == 1:
        sub_box = sub1
        print "The result of S-Box 1 is:"


    print _int_to_bits(sub_box[int(row, 2)][int(col, 2)], 2)
    # return _int_to_bits(sub_box[int(row, 2)][int(col, 2)], 2)

def _int_to_bits(integer, num_bits):
    binary_int = bin(integer)
    binary_result = []
    found = 0
    for i in range(len(binary_int)):
        if found:
            binary_result.append(binary_int[i])

        if binary_int[i] == 'b':
            found = 1

    padding = []
    while len(padding) < num_bits - len(binary_result):
        padding.append('0')

    padding = padding + binary_result
    padding = ''.join(padding)

    return padding


if __name__ == "__main__":
    main()
